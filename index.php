<?php

require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

/*function hello(){
    echo 'Hello World!';
}
Flight::route('/', 'hello');

Flight::route('/hello/@name/@firstname', function($name, $firstname){
    echo "hello, $name $firstname !";
});

/*Flight::route('/first_view/', function(){
    $data = [
        'contenu' => 'Hello World!',
        'name' => 'Ben Kenobi',
    ];
    Flight::view()->display('first_view.twig', $data);
});*/

Flight::route('/books/', function(){
    $data = [
        'books' => getBooks(),
    ];
    Flight::view()->display('books.twig', $data);
});

Flight::start();